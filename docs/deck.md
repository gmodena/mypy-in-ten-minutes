---
theme: gaia
size: 16:9
inlineSVG: true
footer: Wikimedia Foundation, 2021
---

# [SPIKE Investigate Different CI Checks T293382](https://phabricator.wikimedia.org/T293382)
## Python and Static Typing 
### Gabriele Modena <gmodena@wikimedia.org>
---

# Python
* Dynamic language, [duck typing](https://en.wikipedia.org/wiki/Duck_typing)
* [PEP 484 -- Type Hints](https://www.python.org/dev/peps/pep-0484/)
* [PEP 636 -- Structural Pattern Matching](https://www.python.org/dev/peps/pep-0636/?s=09)
* [PEP 646 -- Variadic Generics
](https://www.python.org/dev/peps/pep-0646/)
* Has a lot of momentum in the community 
    * Standard library, major frameworks 
    * More recently Spark + PyData stack

---
# Static Typing with hints
* Used in IDEs, langauge servers
* [mypy](http://mypy-lang.org/), [pyright](https://pypi.org/project/pyright/), ...
 * Promises compile-time checking, easier maintenance (reduce runtime erros, simplify tests), gradual introduction to exsiting code base
* [Duck typing](https://en.wikipedia.org/wiki/Duck_typing) is orthogonal to the type system

--- 
# examples/ex1.py
```python
def mysum(a, b):
    return a + b

mysum('1', '2')
mysum(1, None)
```

---
# examples/ex1.py
```python
def mysum(a, b):
    return a + b

mysum('1', '2') # '12'
mysum(1, None)  # TypeError (at runtime)
```

---
# examples/ex2.py
```python
def mysum(a: int, b: int) -> int:
    return a + b

mysum('1', '2')
mysum(1, None)
```

```bash
$ mypy examples/ex2.py
examples/ex2.py:5: error: Argument 1 to "mysum" has incompatible type "str"; expected "int"
examples/ex2.py:5: error: Argument 2 to "mysum" has incompatible type "str"; expected "int"
examples/ex2.py:6: error: Argument 1 to "mysum" has incompatible type "str"; expected "int"
examples/ex2.py:6: error: Argument 2 to "mysum" has incompatible type "None"; expected "int"
Found 4 errors in 1 file (checked 1 source file)
```

--- 
# [T293382](https://phabricator.wikimedia.org/T293382)
* Add "compile time" type-checking via mypy to data pipelines CI 
* Integrated with `tox`
* Required a couple of [stubs](https://mypy.readthedocs.io/en/stable/stubs.html) deps (`pyspark-stubs`, `numpy-stubs`)
* More details in the 2021-11-18 demo.
--- 

# [T293382](https://phabricator.wikimedia.org/T293382)
```bash
spark/transform.py:5: error: Cannot find implementation or library stub for module named "schema"
spark/transform.py:6: error: Cannot find implementation or library stub for module named "instances_to_filter"
spark/search_table.py:34: error: "Column" not callable
spark/raw2parquet.py:3: error: Cannot find implementation or library stub for module named "schema"
spark/raw2parquet.py:3: note: See https://mypy.readthedocs.io/en/stable/running_mypy.html#missing-imports
spark/raw2parquet.py:32: error: Argument "header" to "options" of "DataFrameReader" has incompatible type "bool";
expected "str"
```

---

# Introducing mypy to our codebase
* Apache Spark has good typing support
 * [pyspark-stubs](https://pypi.org/project/pyspark-stubs/) has been merged in 3.1.
 * [Type Hints in Pandas API on Spark¶
](https://spark.apache.org/docs/latest//api/python/user_guide/pandas_on_spark/typehints.html)
 * [New Pandas UDFs and Python Type Hints in the Upcoming Release of Apache Spark 3.0](https://databricks.com/blog/2020/05/20/new-pandas-udfs-and-python-type-hints-in-the-upcoming-release-of-apache-spark-3-0.html)
* Graudal typing
* Start with annotating method signatures
* Execute `mypy` as an extra CI step 

---

# tox.ini
```
[mypy]
python_version = 3.7 # The only python version we currently support.
disallow_untyped_defs = True # methods signatures should be typed.
disallow_any_unimported = True # disallows usage of types that come from unfollowed imports.
no_implicit_optional = True # <- Explicit is better than implicit. Open to debate :).
check_untyped_defs = True # Type-checks the interior of functions without type annotations.
warn_return_any = True # Shows a warning when returning a value with type Any from a function declared with a non- Any return type.
show_error_codes = True # Show error codes in output.
warn_unused_ignores = True # Warns about unneeded # type: ignore comments.
```

---

# Pain-points
* Requires some discipline, easy to abuse `# type: ignore`
* Some weirdness around collection (`list` vs `typing.List`)
 * Addressed in 3.9
* Exception handling
* Syntax can get cumbersome (e.g. Generics)
* Dealing with C code
* The UX still feels off sometimes. Why not just write new code in Go / Scala?
